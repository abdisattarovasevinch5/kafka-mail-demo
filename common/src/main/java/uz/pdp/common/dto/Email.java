package uz.pdp.common.dto;
//Sevinch Abdisattorova 04/26/2022 9:52 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class Email {
    String to;
    String message;
//    String from;
    String subject;
}

