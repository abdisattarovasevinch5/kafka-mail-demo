package uz.pdp.consumerservice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerService1Application {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerService1Application.class, args);
    }

}
