package uz.pdp.consumerservice1.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import uz.pdp.common.dto.Email;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    @Bean
    public Map<String, Object> configs() {

        HashMap<String, Object> props = new HashMap<>();

        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "1");
        return props;
    }


    @Bean
    public ConsumerFactory<String, Email> consumerFactory() {
        JsonDeserializer<Email> userDtoJsonDeserializer =
                new JsonDeserializer<>(Email.class, false);
        userDtoJsonDeserializer.addTrustedPackages("uz.pdp.common");
        return new DefaultKafkaConsumerFactory<>(
                configs(),
                new StringDeserializer(),
                userDtoJsonDeserializer
        );
    }


    @Bean
    public KafkaListenerContainerFactory<
            ConcurrentMessageListenerContainer<
                    String, Email>>
    messageFactory() {

        ConcurrentKafkaListenerContainerFactory<String, Email> containerFactory =
                new ConcurrentKafkaListenerContainerFactory<>();

        containerFactory.setConsumerFactory(consumerFactory());

        return containerFactory;
    }


    @Bean
    public KafkaListenerContainerFactory<
            ConcurrentMessageListenerContainer<
                    String, Email>>
    emailFactory() {

        ConcurrentKafkaListenerContainerFactory<String, Email> containerFactory =
                new ConcurrentKafkaListenerContainerFactory<>();

        containerFactory.setConsumerFactory(consumerFactory());

        return containerFactory;
    }
}
