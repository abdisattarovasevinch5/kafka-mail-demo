package uz.pdp.consumerservice1.service;
//Sevinch Abdisattorova 04/26/2022 11:10 AM

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.common.dto.Email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


@Service
@RequiredArgsConstructor
public class ConsumerService {

    private final JavaMailSender javaMailSender;

    private final TemplateEngine templateEngine;

    private static final Logger logger =
            LoggerFactory.getLogger(ConsumerService.class);

    @KafkaListener(topics = "test-topic",
            groupId = "1",
            containerFactory = "messageFactory"
    )
    public void receiveMessage(Email email) {
        logger.info("Email received is.. " + email);
        sendEmail(email);
    }

    public void sendEmail(Email email) {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Context context = new Context();
            Map<String, Object> props = new HashMap<>();
            props.put("message", email.getMessage());
            context.setVariables(props);

            helper.setFrom("abdisattarovasevinch5@gmail.com");
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());

            String html = templateEngine.process("email.html", context);
            helper.setText(html, true);

            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }


}
