package uz.pdp.producerservice.controller;
//Sevinch Abdisattorova 04/26/2022 9:10 AM

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.common.dto.Email;
import uz.pdp.producerservice.service.ProducerService;

@RestController
@RequestMapping("/api/email")
@RequiredArgsConstructor
@Slf4j
public class ProducerController {


    private final ProducerService producerService;


    @PostMapping
    public ResponseEntity<?> publish(@RequestBody Email email){
        return producerService.publish(email);
    }


}
