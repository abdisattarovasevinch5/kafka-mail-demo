package uz.pdp.producerservice.service;
//Sevinch Abdisattorova 04/26/2022 9:11 AM

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import uz.pdp.common.dto.Email;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProducerService {


    private final KafkaTemplate<String, Email> kafkaTemplate;


    public ResponseEntity<?> publish(Email email) {

        kafkaTemplate.send("test-topic", email);

        return ResponseEntity.ok("Success!");
    }


}
